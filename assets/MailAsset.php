<?php
/**
 * Created by PhpStorm.
 * User: DNS
 * Date: 03.09.2018
 * Time: 20:31
 */

namespace app\assets;
use yii\web\AssetBundle;

class MailAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css=[
        '/css/tophotels/mail-main.css',
        '/css/site.css'
    ];
    public $js=[
        "/js/libs/array-function.js",
        "/js/libs/date-function.js",
        "/js/libs/number-function.js",
        "/js/libs/string-function.js",
        "/js/libs/debounce.js",
        "/js/libs/reverseLocale.js",
        "/js/libs/LSPager.js",
        "/js/libs/LSSuggest.js"
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}