<?php

namespace app\assets;

use yii\web\AssetBundle;

class TophotelsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "css/tophotels/reset-ls.css",
        "css/fonts.css",
        "css/vendor/font-awesome-5.0/css/fontawesome-all.css",
        "css/vendor/font-awesome-4.7.0/css/font-awesome.min.css",
        "css/vendor/sumoselect.css",
        "css/vendor/magnific-popup.css",
        "css/tophotels/atom.css",
        "css/tophotels/main.css",
        "css/tophotels/paginator.css",
        "css/tophotels/main-cnt.css",
        "css/tophotels/th-sumoselect.css",
        "css/tophotels/layouts/header.css",
        "css/tophotels/layouts/header-mobile.css",
        "css/tophotels/layouts/footer2018.css",
        "css/tophotels/processing-agreement-pp.css",
        "css/tophotels/form-durability.css",
        "css/tophotels/form-date.css",
        "css/tophotels/form-adults.css",
        "css/tophotels/form-direction.css",
        "css/tophotels/hotels-catalog.css",
        "css/tophotels/registration-form.css",
        "css/tophotels/orders-line.css",
        "css/tophotels/order-form.css",
        "css/site.css"

    ];

public $js = [
    "/js/vendor/datepicker-jquery-ui-1.12.1.custom/jquery-ui.min.js",
    "/js/vendor/slick.js",
    "/js/vendor/gemini-calendar.js",
    "/js/vendor/SumoSelectLS/js/jquery.sumoselect.min.js",
    "/js/tophotels/header-mobile.js",
    "/js/tophotels/agree-pp.js",
    "/js/tophotels/legal-inform-pp.js",
    "/js/libs/date-function.js",
    "/js/vendor/magnific-popup.min.js",
    "/js/datepicker.js",
    "/js/tophotels/form-directions.js",
    "/js/form.js",
    "/js/tophotels/help-selection.js",
    "/js/tophotels/main.js",
];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}