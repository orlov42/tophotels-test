<?php

namespace app\controllers;

use Yii;
use app\models\Order;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Consultant;



class AdminController extends Controller
{
    public $layout = 'admin';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    //отображение всех заказов
    public function actionIndex()
    {
        //выбор необходимого сценария валидации полей
        //объект Order создается для последующей передачи в качестве параметра в GridView
        $searchModel = new Order(['scenario' => Order::SCENARIO_ADMIN]);

        //создание dataProvider производится из параметров запроса с помощью метода модели Order
        $dataProvider = $searchModel->createDataProvider(Yii::$app->request->queryParams);

        //установка пагинации
        $dataProvider->pagination = [
            'pageSize' => 10,
        ];

        //строится необходимый вид с параметрами
        return $this->render('index', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    //отображение одного конкретного заказа
    //$id - ID конкретной модели
    public function actionView($id)
    {
        try {
            $model = $this->findModel($id);
        } catch (NotFoundHttpException $e) {

            //в случае ввода несуществующего ID - переход на страницу со всеми заказами
            return $this->redirect(['admin/index']);
        }

        return $this->render('view', [
            'model' => $model,
        ]);

    }

    //создание новой модели
    //при успешном создании происходит перенаправление на страницу просмотра
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    //обновление существующей модели
    //$id - ID конкретной модели
    //при успешном обновлении - редирект на страницу с подробной информацией
    public function actionUpdate($id)
    {
        try {
            $model = $this->findModel($id);
        } catch (NotFoundHttpException $e) {
            return $this->redirect(['index']);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    //удаление существующей модели
    //$id - ID конкретной модели
    public function actionDelete($id)
    {
        try {
            $model = $this->findModel($id);
        } catch (NotFoundHttpException $e) {
            return $this->redirect(['index']);
        }
        $model->delete();

        return $this->redirect(['index']);
    }


    //функция для поиска модели
    //$id - ID конкретной модели
    //выбрасывает исключение если не удается найти модель
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('Запрашиваемая страница не найдена');
    }

    public function actionConsultants(){
        $consultants = Consultant::find()->all();
        return $this->render('consultants', compact('consultants'));
    }

}
