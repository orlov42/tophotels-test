<?php

namespace app\controllers;

use Yii;
use yii\db\Exception;
use yii\web\Controller;
use app\models\Order;
use app\models\Consultant;

class SiteController extends Controller
{
    //метод для отображения главной страницы
    public function actionIndex()
    {
        $model = new Order();

        //список стран собирается из Postgre
        $countries = Yii::$app->pgdb
            ->createCommand(
                'SELECT dict_country.id, dict_country.name_eng, dict_country.name, 
                COUNT(dict_resort.id) as cities FROM dict.dict_country, dict.dict_resort 
                WHERE dict_resort.country = dict_country.id 
                AND dict_country.active = true
                GROUP BY dict_country.id
                ORDER BY dict_country.name'
            )->queryAll();

        //данные из ActiveForm загружаются в модель
        if ($model->load(Yii::$app->request->post())) {

            //подготовка команды SQL
            $command = Yii::$app->db->createCommand()->insert('orders_new', [
                'name' => $model->name,
                'country' => $model->country,
                'description' => $model->description,
                'phone' => $model->phone,
                'email' => $model->email,
                'consultant' => null
            ]);
            try {
                $command->execute();
                $id = Yii::$app->db->getLastInsertID();

            } catch (Exception $e) {
                Yii::$app->session->setFlash('error');
                return $this->refresh();
            }

            //отправка email для простой формы осуществляется через метод самой модели
            $model->sendEmail(['id' => $id, 'name' => $model->name, 'phone' => $model->phone, 'email' => $model->email]);

            Yii::$app->session->setFlash('submit-success');
            return $this->refresh();
        } else {
            return $this->render('index', compact('model', 'countries'));
        }
    }

    //роут для отображения ошибки
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }

    public function actionTest()
    {
        $action = Yii::$app->request->post('action');

        //обращение к Postgre для сбора данных о городах (резортах)
        //возвращает json со всеми городами
        if ($action == 'getCities') {
            $cities = Yii::$app->pgdb->createCommand(
                "
                SELECT dict_resort.id, dict_resort.name, dict_resort.name_eng, 
                COUNT(dict_allocation.id) as count_hotels
                FROM dict.dict_resort, dict.dict_allocation
                WHERE country = :countryID
                AND dict_allocation.resort = dict_resort.id
                GROUP BY(dict_resort.id)
                ORDER BY name"
            )
                //значение выбранной страны подставляется из запроса
                ->bindValue(':countryID', $_POST['countryID'])
                ->queryAll();
            return json_encode($cities);
        }
        //обращение к Postgre для сбора данных об отелях
        if ($action == 'getHotels') {
            $hotels = Yii::$app->pgdb->createCommand(
                "SELECT id, name FROM dict.dict_allocation
                    WHERE resort = :resortID"
            )
                //значение выбранного резорта подставляется из запроса
                ->bindValue(':resortID', $_POST['resortID'])
                ->queryAll();
            return json_encode($hotels);
        }

        if ($action == 'createId') {
            $name = Yii::$app->request->post('name');
            $email = Yii::$app->request->post('email');
            $phone = Yii::$app->request->post('phone');

            $command = Yii::$app->db->createCommand()->insert('orders_new', [
                'name' => $name,
                'email' => $email,
                'phone' => $phone
            ]);

            try {
                $command->execute();
                $id = Yii::$app->db->getLastInsertID();

            } catch (Exception $e) {
                Yii::$app->session->setFlash('error');
                return $this->refresh();
            }
//            $userData = compact('id', 'name', 'email', 'phone');
//
//            Yii::$app->mailer->compose('confirm-mail', compact('userData'))
//                ->setFrom([Yii::$app->params['adminEmail'] => 'TopHotels'])
//                ->setTo(Yii::$app->params['adminEmail'])
//                ->setSubject('Добавлена новая заявка')
//                ->setTextBody('Поступила заявка №' . $id)
//                ->send();
            return $id;
        }

        //дополнение заявки
        if ($action == 'updateOrder') {
            $id = Yii::$app->request->post('id');

            $command = Yii::$app->db->createCommand()
                ->update('orders_new', [
                    'startDate' => $_POST['startDate'],
                    'endDate' => $_POST['endDate'],
                    'counterDays' => $_POST['counterDays'],
                    'nightStart' => $_POST['nightStart'],
                    'nightEnd' => $_POST['nightEnd'],
                    'adults' => $_POST['adults'],
                    'children' => $_POST['children'],
                    'vacation_type' => $_POST['vacationType'],
                    'currency' => $_POST['currency'],
                    'money' => $_POST['money'],
                    'name' => $_POST['name'],
                    'email' => $_POST['email'],
                    'phone' => $_POST['phone'],
                ], ['id' => $id]);

            //попытка обновления данных в БД
            try {
                $command->execute();
                // страница обновляется и выводится сообщение об ошибке
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error');
                return $this->refresh();
            }
        }

        //финальное заполнение заявки
        if ($action == 'finalizeOrder') {
            $id = Yii::$app->request->post('id');

            $order = Order::find()->where(['id' => $id])->one();

            $dataArray = [
                'id' => $id,
                'name' => $order->name,
                'email' => $order->email,
                'phone' => $order->phone,
                'country' => $_POST['countryID'],
                'resortID' => $_POST['resortID'],
                'hotelID' => $_POST['hotelID'],
                'star5' => $_POST['star5'],
                'star4' => $_POST['star4'],
                'star3' => $_POST['star3'],
                'star2' => $_POST['star2']
            ];

            $command = Yii::$app->db->createCommand()
                ->update('orders_new', [
                    'country' => $_POST['country'],
                    'countryID' => $_POST['countryID'],
                    'resort' => $_POST['resort'],
                    'hotel' => $_POST['hotel'],
                    'hotelID' => $_POST['hotelID'],
                    'other_directions' => $_POST['otherDirections'],
                    'firstBeachLine' => $_POST['firstBeachLine'],
                    'allInclusive' => $_POST['allInclusive'],
                    'animation' => $_POST['animation'],
                    'parties' => $_POST['parties'],
                    'childPot' => $_POST['childPot'],
                    'childFood' => $_POST['childFood'],
                    'childTable' => $_POST['childTable'],
                    'childAnimation' => $_POST['childAnimation'],
                    'addComment' => $_POST['addComment'],
                    'userCity' => $_POST['userCity'],
                    'userSubway' => $_POST['userSubway'],
                    'hotelRate' => $_POST['hotelRate'],
                    'hotelReviews' => $_POST['hotelReviews'],
                    'hotelRate2years' => $_POST['hotelRate2years'],
                    'star5' => $_POST['star5'],
                    'star4' => $_POST['star4'],
                    'star3' => $_POST['star3'],
                    'star2' => $_POST['star2'],

                    'consultant' => $this->sendEmail($dataArray)
                ], ['id' => $id]);

            //попытка обновления данных в БД
            try {
                $command->execute();
                // страница обновляется и выводится сообщение об ошибке
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error');
                return $this->refresh();
            }
        }
    }


    protected function sendEmail($data)
    {
        $managerName = 'Анна';
        $managerEmail = 'test.th.welcome@gmail.com';

        if ($data['country'] != '') {
            $manager = Consultant::find()->where(['countryID' => $data['country']])->one();
            if($manager){
                //проверка на resort
                if (isset($manager->resortID)) {
                    if ($manager->resortID == $data['resortID']) {
                        $managerName = $manager->name;
                        $managerEmail = $manager->email;
                    }
                }

                if( isset($manager->rating)){
                    //выясняем, с какими рейтингами он работает
                    $ratings = explode(',', $manager->rating);
                    foreach ($ratings as $rating) {
                        $ratingName = 'star' . $rating;
                        //если в запросе есть нужный нам рейтинг то отправляем письмо на почту менеджера
                        if ($data[$ratingName] != 0) {
                            $managerName = $manager->name;
                            $managerEmail = $manager->email;
                            break;
                        }
                    }
                }
            }


            $data['managerName'] = $managerName;
            $data['managerEmail'] = $managerEmail;

            Yii::$app->mailer->compose('confirm-mail', compact('data'))
                ->setFrom([Yii::$app->params['adminEmail'] => 'TopHotels'])
                ->setSubject('Добавлена новая заявка')
                ->setTextBody('Поступила заявка №' . $data['id'])
                ->setTo($managerEmail)
                ->send();

        } else {
            $managerName = 'Нет распределения, так как нет направления';
        }
        return $managerName;
    }
}
