<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;


/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $description
 * @property string $country
 * @property string $timestamp
 */
class Order extends \yii\db\ActiveRecord
{

    const SCENARIO_ADMIN = 'admin';

    public static function tableName()
    {
        return 'orders_new';
    }

    //функция для склонения существительных
    //в качестве параметров принимает разные формы слова и число ('город', 'городов', 'города', 3)
    //возвращает верную форму для конкретного числа
    protected function declination($a, $b, $c, $s)
    {
        $words = [$a, $b, $c];
        $days = $s % 100;
        if ($days >= 11 && $days <= 14)
            $days = 0;
        else
            $days = ($days %= 10) < 5 ? ($days > 2 ? 2 : $days) : 0;
        return $words[$days];
    }

    //геттер для отображения бюджета
    public function getBudget()
    {
        return $this->money . ' ' . $this->currency;
    }

    //геттер для отображения отображения ночей в формате 1-5
    public function getNights()
    {
        return $this->nightStart . ' - ' . $this->nightEnd;
    }

    //геттер для создания целого названия для направления (Страна/резорт/отель)
    public function getFullDestination()
    {
        $result = null;
        if ($this->country)
            $result = $this->country;
        if ($this->resort)
            $result .= ' / ' . $this->resort;
        if ($this->hotel)
            $result .= ' / ' . $this->hotel;

        return $result;
    }

    //геттер для отображения проставленного рейтинга отеля
    public function getHotelStars()
    {
        $result = '';
        if ($this->star2) {
            $result .= '2 звезды;';
        }
        if ($this->star3) {
            $result .= '3 звезды;';
        }
        if ($this->star4) {
            $result .= '4 звезды;';
        }
        if ($this->star5) {
            $result .= '5 звезд;';
        }
        $result .= $this->hotelRate;
        $result = ($result == '') ? null : $result;

        return $result;
    }

    //геттер для получения информации о городе заказчика
    public function getUserCity()
    {
        if (!$this->userCity)
            return null;
        return $this->userCity;
    }

    //геттер для корректного отображения колонки "Рассмотрю другие направления)
    public function getOtherDirections()
    {
        if ($this->other_directions == 1)
            return 'Да';
        else
            return 'Нет';
    }

    //геттер для формирования "всех остальных" полей сложной формы заявки
    public function getAllComments()
    {
        $result = '';
        if ($this->adults)
            $result .= $this->adults . ' ' . $this->declination('взрослых', 'взрослый', 'взрослых', $this->adults) . '; ';
        if ($this->children)
            $result .= $this->children . ' ' . $this->declination('детей', 'ребенок', 'детей', $this->children) . '; ';
        if ($this->firstBeachLine == 1)
            $result .= 'Первая линия пляжа; ';
        if ($this->allInclusive == 1)
            $result .= 'Питание всё включено; ';
        if ($this->animation == 1)
            $result .= 'Весёлая анимация; ';
        if ($this->parties == 1)
            $result .= 'Вечеринки рядом с отелем; ';
        if ($this->childPot == 1)
            $result .= 'Детский горшок; ';
        if ($this->childFood == 1)
            $result .= 'Детские блюда; ';
        if ($this->childTable == 1)
            $result .= 'Пеленальный столик; ';
        if ($this->childAnimation == 1)
            $result .= 'Анимация; ';
        if ($this->addComment)
            $result .= 'Комментарий: ' . $this->addComment . '; ';
        if ($this->userSubway)
            $result .= 'Метро: ' . $this->userSubway . '; ';
        return $result;
    }

    //функция для отправки email
    //в качестве параметров принимает массив с данными
    public function sendEmail($data)
    {
        Yii::$app->mailer->compose('confirm-mail', compact('data'))
            ->setFrom([Yii::$app->params['adminEmail'] => 'TopHotels'])
            ->setTo(Yii::$app->params['adminEmail'])
            ->setSubject('Добавлена новая заявка')
            ->setTextBody('Поступила заявка №' . $data['id'])
            ->send();
    }

    //сценарии для валидирования полей
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        return $scenarios;
    }

    //правила валидации полей
    public function rules()
    {
        return [
            [['name', 'email', 'phone', 'description', 'country'], 'required', 'on' => [self::SCENARIO_DEFAULT]],
            [['timestamp', 'money', 'currency', 'nightStart', 'nightEnd'], 'safe', 'on' => [self::SCENARIO_DEFAULT]],

            [['id'], 'integer', 'on' => [self::SCENARIO_ADMIN]],
            [['name', 'email', 'phone', 'description', 'country', 'timestamp', 'vacation_type'], 'safe', 'on' => [self::SCENARIO_ADMIN]]
        ];
    }

    //формирование собственных названий для полей
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Ваше имя',
            'email' => 'Email',
            'phone' => 'Телефон',
            'description' => 'Описание',
            'timestamp' => 'Время заказа',
            'country' => 'Направление',
            'budget' => 'Бюджет',
            'nights' => 'Ночи',
            'fullDestination' => 'Направление',
            'hotelStars' => 'Категория отеля',
            'vacation_type' => 'Тип отдыха',
            'hotelRate' => 'Рейтинг отеля',
            'userCity' => 'Город',
            'otherDirections' => 'Другие направления?',
            'allComments' => 'Дополнения',
            'consultant' => 'Консультант',
        ];
    }

    //функция для создания dataProvider из параметров запроса
    public function createDataProvider($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'vacation_type', $this->vacation_type]);

        return $dataProvider;
    }


}
