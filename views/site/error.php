<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= $message ?>
        <?= $name ?>
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <h1 style="text-align: center;font-size:20px;color:red">
        Во время обработки запроса произошла ошибка.
    </h1>

</div>
