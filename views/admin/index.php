<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1 style="font-size:30px;text-align: center"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать заявку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <p>
        <?= Html::a('Консультанты', ['consultants'], ['class' => 'btn btn-info mt10 mb10', 'target'=>'_blank']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'columns' => [
            ['attribute' => 'id', 'label' => 'ID заявки', 'encodeLabel' => false,],
            ['attribute' => 'timestamp', 'label' => 'Дата и время добавления', 'encodeLabel' => false,],
            ['attribute' => 'fullDestination', 'label' => 'Направление', 'encodeLabel' => false,],
            ['attribute' => 'name', 'label' => 'Имя', 'encodeLabel' => false,],
            ['attribute' => 'phone', 'label' => 'Телефон', 'encodeLabel' => false,],
            ['attribute' => 'email', 'label' => 'Email', 'encodeLabel' => false,],
            ['attribute' => 'vacation_type', 'label' => 'Тип отдыха', 'encodeLabel' => false,],
            ['attribute' => 'budget', 'label' => 'Бюджет', 'encodeLabel' => false,],
//            ['attribute' => 'nights', 'label' => 'Ночи', 'encodeLabel' => false,],
//            ['attribute' => 'hotelStars', 'label' => 'Звездность/рейтинг', 'encodeLabel' => false,],
//            ['attribute' => 'hotelRate', 'label' => 'Рейтинг отеля', 'encodeLabel' => false,],
//            ['attribute' => 'userCity', 'label' => 'Город', 'encodeLabel' => false,],
//            ['attribute' => 'otherDirections', 'label' => 'Др.направления', 'encodeLabel' => false,],
//            ['attribute' => 'allComments', 'label' => 'Дополнения', 'encodeLabel' => false,],
            ['attribute' => 'consultant', 'label' => 'Консультант', 'encodeLabel' => false,],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
