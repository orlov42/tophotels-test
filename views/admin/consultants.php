<?php
/**
 * Created by PhpStorm.
 * User: DNS
 * Date: 10.09.2018
 * Time: 14:28
 */
?>

<table class="table table-striped table-bordered">
    <thead>
    <th>Имя</th>
    <th>Email</th>
    <th>Страна</th>
    <th>Resort</th>
    <th>Звездность</th>
    </thead>
    <tbody>
    <?php foreach ($consultants as $consultant): ?>
        <tr>
            <td><?= $consultant->name ?></td>
            <td><?= $consultant->email ?></td>
            <td><?= $consultant->countryName ?>

                <?php if(isset($consultant->countryID)): ?>
                (<?= $consultant->countryID ?>)
                <?php endif ?>

            </td>
            <td><?= $consultant->resortName ?>
                <?php if(isset($consultant->resortID)): ?>
                    (<?= $consultant->resortID ?>)
                <?php endif ?>
            </td>
            <td><?= $consultant->rating ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>