<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    label.label{
        font-size:15px;
        color:black;
        padding: 0;
    }
    .bth__inp-block{
        margin-top: 5px !important;
    }
    .form-group{
        margin-top: 10px;
        margin-bottom: 20px;
    }
</style>
<div class="order-form">

    <?php $form = ActiveForm::begin([
        'fieldConfig' => [
            'inputOptions' => ['class' => 'bth__inp bold'],
            'labelOptions' => ['class' => 'label'],
            'template' => "{label}<div class=\"bth__inp-block long mt20\">{input}</div>",
//            'options' => ['class' => 'bth__inp-block mt30']
        ],
    ]); ?>

    <?= $form->field($model, 'name')->input('text') ?>

    <?= $form->field($model, 'email')->input('email') ?>

    <?= $form->field($model, 'phone')->input('text') ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
