
var initGeminiDPicker = function(id, onchange) {
    var datePicker1 = $('#' + id);
    var pickerObj1 = datePicker1.datepicker({
        type: 'date-range',
        format: 'yyyy-MM-dd',
        align: 'center',
        lang: 'ru-RU',
        weekStart: 1,
        defaultValue: ((new Date()).format('Y-m-d') + ' - ' + (new Date()).addDays(12).format('Y-m-d')),
        startDate: new Date(),
        endDate: (new Date()).addMonths(12),
        pickerBlockId: 'js-dpicker-filter-' + id,
        onChange: function (newValue) {
            onchange(newValue);
        }
    });

    pickerObj1.show();
    $('#js-dpicker-filter-' + id)
        .css('max-width', '520px')
        .addClass('hidden')
        .insertAfter(datePicker1.parent())
        .find('.gmi-picker-panel')
        .show();

    datePicker1.parent().click(function(){
        $('#js-dpicker-filter-' + id).toggleClass('hidden');   //ss1
    });
    datePicker1.click(function(){
        $('#js-dpicker-filter-' + id).toggleClass('hidden');   //ss1
    });

};

var initGeminiDPickerSingle = function(id, onchange) {
    var datePicker1 = $('#' + id);
    var pickerObj1 = datePicker1.datepicker({
        type: 'date',
        format: 'yyyy-MM-dd',
        align: 'center',
        lang: 'ru-RU',
        weekStart: 1,
        defaultValue: ((new Date()).format('Y-m-d') + ' - ' + (new Date()).addMonths(12).format('Y-m-d')),
        startDate: new Date(),
        endDate: (new Date()).addMonths(12),
        pickerBlockId: 'js-dpicker-filter-' + id,

        onChange: function (newValue) {
            onchange(newValue);
        }
    });
    pickerObj1.show();
    $('#js-dpicker-filter-' + id)
        .css('max-width', '520px')
        .addClass('hidden')
        .insertAfter(datePicker1.parent())
        .find('.gmi-picker-panel')
        .show();

    datePicker1.parent().click(function(){
        $('#js-dpicker-filter-' + id).toggleClass('hidden');
    });
};



