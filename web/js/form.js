//функция для склонения существительных
//в качестве параметров принимает разные формы слова и число ('город', 'городов', 'города', 3)
//возвращает верную форму для конкретного числа
function declination(a, b, c, s) {
    let words = [a, b, c];
    let days = s % 100;

    if (days >= 11 && days <= 14) {
        days = 0;
    }
    else {
        days = (days %= 10) < 5 ? (days > 2 ? 2 : days) : 0;
    }
    return (words[days]);
}

//счетчик для отслеживания кликов (step=1 - старт, step=2 - конец)
var step = 1;
//объект, в котором хранится вся информация по создаваемой заявке
var orderData = {
    orderId: '',
    startDate: (new Date()).format('Y-m-d'),
    endDate: (new Date()).addDays(12).format('Y-m-d'),
    counterDays: 14,
    // totalNightsStart: 1,
    // totalNightsEnd: 28,
    fieldNightStart: 1,
    fieldNightEnd: 14,
    adults: 2,
    children: 0,
    vacationType: 'other',
    currency: 'RUB',
    money: '',
    name: '',
    phone: '',
    email: '',
    country: '',
    countryID: '',
    resort: '',
    resortID: '',
    hotel: '',
    hotelID: '',
    otherDirections: 0,
    firstBeachLine: '',
    allInclusive: '',
    animation: '',
    parties: '',
    star2: '',
    star3: '',
    star4: '',
    star5: '',
    hotelRate: '',
    hotelReviews: '',
    hotelRate2years: '',
    childPot: '',
    childFood: '',
    childTable: '',
    childAnimation: '',
    addComment: '',
    userCity: '',
    userSubway: '',
};

$(document).ready(function () {
    let citiesArray;

    // var step = 1;

    //на каком шаге сейчас форма заявки (сложная)
    let formStep = 1;



    //функция для смены количества дней отдыха
    function changeHotelDays() {

        //блок, в котором строятся ночи
        let nights = document.getElementById('for_append')
        nights.innerHTML = '';

        //создание интерактивных выделяемых блоков для ночей

        for (let i = 1; i < 29; i++) {
            let div = document.createElement('div');
            switch (i) {
                case (orderData['fieldNightStart']):
                    div.className = "form-durability__select-item js-duration-cell start";
                    break;
                case (orderData['fieldNightEnd']):
                    div.className = "form-durability__select-item js-duration-cell end";
                    break;
                default:
                    if ((i > orderData['fieldNightStart']) && (i < orderData['fieldNightEnd'])) {
                        div.className = "form-durability__select-item js-duration-cell selected";
                    } else {
                        div.className = "form-durability__select-item js-duration-cell";
                    }
                    break;
            }
            div.innerHTML = i;
            $(div).click(function () {
                if (step === 1) {
                    $(".form-durability__select-item").removeClass('start')
                        .removeClass('end')
                        .removeClass('selected');
                    $(this).addClass('start');
                    orderData['fieldNightStart'] = $(this).text();
                    step = 2;
                } else if (step === 2) {
                    if (parseInt($(this).text()) > orderData['fieldNightStart']) {
                        $(this).addClass('end');
                        orderData['fieldNightEnd'] = $(this).text();
                        let nightDeclination = declination("ночей", "ночь", "ночи", orderData['fieldNightEnd']);
                        $('#nightCounter').text(orderData['fieldNightStart'] + ' - ' + orderData['fieldNightEnd'] + ' ' + nightDeclination);
                        console.log('func '+step)
                        $('.formDirections').hide();
                        step = 1;
                    }
                    else if($(this).text() === orderData['fieldNightStart']){
                        orderData['fieldNightEnd'] = $(this).text();
                        let nightDeclination = declination("ночей", "ночь", "ночи", orderData['fieldNightEnd']);
                        $('#nightCounter').text(orderData['fieldNightEnd'] + ' ' + nightDeclination);
                        $('.formDirections').hide();
                        step = 1;
                    }
                    else {
                        $(".form-durability__select-item").removeClass('start')
                            .removeClass('end')
                            .removeClass('selected');
                        $(this).addClass('start');
                        orderData['fieldNightStart'] = $(this).text();
                        step = 2;
                    }
                }
            });

            //анимация выделения ночей
            $(div).mouseenter(function () {
                if (step === 2) {
                    $(this).addClass('end');
                    let self = $(this);
                    if (parseInt($(this).text()) > orderData['fieldNightStart']) {
                        $(".form-durability__select-item").each(function (i, elem) {
                            if (i >= orderData['fieldNightStart']) {
                                $(elem).removeClass('selected');
                                if (i < ((parseInt($(self).text())) - 1)) {
                                    $(elem).addClass('selected');
                                }
                            }
                        });
                    } else {
                        $(".form-durability__select-item").each(function (i, elem) {
                            $(elem).removeClass('selected');
                        });
                    }
                }
            });
            $(div).mouseout(function () {
                if (step === 2) {
                    $(".form-durability__select-item")
                        .removeClass('end')
                        .each(function (i, elem) {
                            $(elem).removeClass('selected');
                        });
                }
            });

            nights.appendChild(div);
        }

        let nightDeclination = declination("ночей", "ночь", "ночи", orderData['fieldNightEnd']);

        $('#nightCounter').text(orderData['fieldNightStart'] + ' - ' + orderData['fieldNightEnd'] + ' ' + nightDeclination);
    }

    $(datePicker1).on('pick.datepicker', function (event) {
        let re = /\s* - \s*/
        let dateRange = event.newDate.split(re);
        let dayOfWeekStart = ["вс", "пн", "вт", "ср", "чт", "пт", "сб"][new Date(dateRange[0]).getDay()];
        let date1 = new Date(dateRange[0]);
        let date2 = new Date(dateRange[1]);
        let howDays = Math.ceil(Math.abs(date2.getTime() - date1.getTime()) / (1000 * 3600 * 24));
        let nowDate = new Date(dateRange[0]);
        let options = {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric',
        };
        nowDate = nowDate.toLocaleString("ru", options);

        let dayDeclination = declination("дней", "день", "дня", howDays);

        let finalDateString = dayOfWeekStart + ' ' + nowDate + ' <span class="fz14 normal"> + ' + howDays + ' ' + dayDeclination + '</span>';

        orderData['startDate'] = dateRange[0];
        orderData['endDate'] = dateRange[1];

        // orderData['totalNightsEnd']
        //     = orderData['fieldNightEnd']
        //     = orderData['counterDays']
        //     = howDays + 1;
        // orderData['fieldNightStart'] = 1;
        orderData['counterDays'] = howDays + 1
        changeHotelDays();

        if (dayOfWeekStart) {
            $("#datePicker1").html(finalDateString);
        } else {
            $("#datePicker1").text("Выберите дату");
        }
    });


    $('.bth__inp-city').hide();
    $('.bth__inp-flag').hide();
    $('.bth__inp-country').hide();
    $('.close_what').hide();
    $('.js-act-city').hide();
    $('.js-act-hotels').hide();
    $('.js-act-country').addClass('active');
    $('.otherWays').hide();
    $('.forKids').hide();
    $(".chb_firstLine").removeClass('block');


    $('.order-form-label[data-type]').click(function () {
        orderData['vacationType'] = $(this).data('type')
    });


    let email_regex = /^[a-z][a-zA-Z0-9_]*(\.[a-zA-Z][a-zA-Z0-9_]*)?@[a-z][a-zA-Z-0-9]*\.[a-z]+(\.[a-z]+)?$/;


    //обработка нажатия для перехода к доп. информации расширенной формы
    $('#show-step2').click(function () {

        orderData['name'] = $('#Paramname').val();
        orderData['phone'] = $('#Paramphone').val();
        orderData['email'] = $('#Parammail').val();
        orderData['money'] = $(".currency input").val() | 0;

        let validatedFields = 0;

        if (orderData['name'] === '') {
            $('#Paramname').parent('.js-add-error').addClass('has-error');
        } else {
            $('#Paramname').parent('.js-add-error').removeClass('has-error');
            validatedFields++;
        }

        if (orderData['email'] === '' || !email_regex.test(orderData['email'])) {
            $('#Parammail').parent('.js-add-error').addClass('has-error');
        } else {
            $('#Parammail').parent('.js-add-error').removeClass('has-error');
            validatedFields++;
        }

        if (orderData['phone'] === '') {
            $('#Paramphone').parent('.js-add-error').addClass('has-error');

        } else {
            $('#Paramphone').parent('.js-add-error').removeClass('has-error');
            validatedFields++;
        }

        //если все поля валидны отправляется запрос на создание заявки и осуществляется переход к следующему шагу
        if (validatedFields === 3) {
            if (formStep === 1) {
                sendDataStage2();
                formStep = 2;
                updateStep2Info();
            }
            else if (formStep === 2) {
                updateStep2Info();
            }
        }
    });


    //работа с выпадающим списком валюты
    $(".order__inp-currency-item").click(function () {
        let selectClass;
        let nowClass;

        if ($('.order__inp-currency-wrap .date-text i').hasClass('fa-eur')) {
            nowClass = 'fa-eur';
        }
        else if ($('.order__inp-currency-wrap .date-text i').hasClass('fa-usd')) {
            nowClass = 'fa-usd';
        }
        else {
            nowClass = 'fa-rub';
        }
        let temp_curr = ''
        if ($(this).children('i').hasClass('fa-eur')) {
            selectClass = 'fa-eur';
            temp_curr = 'EUR'
        } else if ($(this).children('i').hasClass('fa-usd')) {
            selectClass = 'fa-usd';
            temp_curr = 'USD'

        } else {
            selectClass = 'fa-rub';
            temp_curr = 'RUB'

        }

        $(this).children('i')
            .removeClass(selectClass)
            .addClass(nowClass);


        $('.order__inp-currency-wrap .date-text i')
            .removeClass(nowClass)
            .addClass(selectClass);

        //выбранная валюта записывается в заявку
        orderData['currency'] = temp_curr;
    });


    //работа с выпадающими списками взрослых и детей
    $('.form-adults__item-btn:first-of-type').click(function () {
        let md = $(".form-adults__item-md");
        switch (md.text()) {
            case '5 взрослых':
                md.text('4 взрослых');
                orderData['adults'] = 4;
                break;
            case '4 взрослых':
                md.text('3 взрослых');
                orderData['adults'] = 3;
                break;
            case '3 взрослых':
                md.text('2 взрослых');
                orderData['adults'] = 2;
                break;
            case '2 взрослых':
                md.text('1 взрослый');
                orderData['adults'] = 1;
                break
        }
    });

    $('.form-adults__item-btn:last-of-type').click(function () {
        let md = $(".form-adults__item-md");
        switch (md.text()) {
            case '1 взрослый':
                md.text('2 взрослых');
                orderData['adults'] = 2;
                break;
            case '2 взрослых':
                md.text('3 взрослых');
                orderData['adults'] = 3;
                break;
            case '3 взрослых':
                md.text('4 взрослых');
                orderData['adults'] = 4;
                break;
            case '4 взрослых':
                md.text('5 взрослых');
                orderData['adults'] = 5;
                break
        }
    });

    //при закрытии сообщения обновляется информация о людях в номере
    $('.form-adults__close').click(function () {
        let md = $(".form-adults__item-md");
        let finishField = $('#peopleCounter');
        let adultCount = md.text()

        let childCount = 0;

        if ($(".form-adults .age1 .SumoSelect > .CaptionCont > span").text() !== ' Добавить ребенка') {
            childCount++;
        }
        if ($(".age2").css('display') !== 'none') {
            childCount++;
        }
        if ($(".age3").css('display') !== 'none') {
            childCount++;
        }

        switch (childCount) {
            case 0:
                finishField.text(adultCount);
                orderData['children'] = 0;
                break;
            case 1:
                finishField.text(adultCount + ", один ребёнок");
                orderData['children'] = 1;
                break;
            case 2:
                finishField.text(adultCount + ", двое детей");
                orderData['children'] = 2;
                break;
            case 3:
                finishField.text(adultCount + ", трое детей");
                orderData['children'] = 3;
                break;
        }
    });

    //возврат к первому шагу формы
    $(".output-block__top").click(function () {
        $("#step2Panel").slideUp(300);
        $("#step1Panel").slideDown(300);
    });

    //построение второго шага сложной формы (с учетом данных из первого шага)
    function updateStep2Info() {
        $("#step1Panel").slideUp(300);
        $("#step2Panel").slideDown(300);
        let vacationType;
        let currency;

        switch (orderData['vacationType']) {
            case "beach":
                vacationType = "ПЛЯЖНЫЙ ОТДЫХ";
                break;
            case "tour":
                vacationType = "ЭКСКУРСИОННЫЙ";
                break;
            case "ski":
                vacationType = "ГОРНОЛЫЖНЫЙ";
                break;
            case "other":
                vacationType = "ДРУГОЕ";
                break;
        }
        switch (orderData['currency']) {
            case "RUB":
                currency = "Рублей";
                break;
            case "EUR":
                currency = "Евро";
                break;
            case "USD":
                currency = "Долларов";
                break;
        }

        let startDate = new Date(orderData['startDate']);
        let endDate = new Date(orderData['endDate']);
        let options = {day: 'numeric', month: 'numeric', year: 'numeric'};
        startDate = startDate.toLocaleString("ru", options);
        endDate = endDate.toLocaleString("ru", options);

        $(".output-block__item").html("<div class=\"output-block__item-block\">" +
            "<b class=\"fz15 mr5\">" + startDate + " - " + endDate + "</b>" +
            "<span class=\"fz13\">даты вылета</span>" +
            "</div><div class=\"output-block__item-block\">" +
            "<b class=\"fz15 mr5\">" + $('#nightCounter').text() + "</b>" +
            "<span class=\"fz13\">пребывание</span>" +
            "</div><div class=\"output-block__item-block\">" +
            "<b class=\"fz15 mr5\">" + $('#peopleCounter').text() + "</b>" +
            "<span class=\"fz13\">человек в номере</span>" +
            "</div>");

        //блок с бюджетом показывается только при наличии заполненных полей на предыдущем шаге формы
        if (orderData['money'] !== 0)
            $(".output-block__top").html("<span class=\"uppercase mr10\">" + vacationType + "</span><span class=\"mr10\">" + orderData['money'] + " " + currency + "</span>");
        else
            $(".output-block__top").html("<span class=\"uppercase mr10\">" + vacationType + "</span>");


        //блок с условиями для детей показывается только при их наличии
        if (orderData['children'] > 0)
            $(".forKids").show();
        else
            $(".forKids").hide();

        //показ "Первая линия пляжа" если выбран соответствующий тип отдыха
        if (orderData['vacationType'] === 'beach')
            $(".chb_firstLine").show();
        else
            $(".chb_firstLine").hide();
    }

    $('.js-show-flag').on('click', function () {
        let regular = /country-.+/
        let countryName = $(this).children('.formDirections__city').text();
        let imageName;
        let arrClass = $(this).attr("class").split(' ');
        let countryID = $(this).data('country');
        orderData['countryID'] = countryID;
        arrClass.forEach(function (t) {
            let result = t.match(regular);
            if (result) {
                let temp = result[0].split("country-");
                if (temp[1]) {
                    imageName = temp[1].toLowerCase();
                }
            }
        });

        orderData['country'] = countryName;

        $('#cty_pull').html("");

        //отправка запроса для построения списка городов (по стране)
        $.ajax({
            url: '/web/index.php? r=site/test',
            type: 'post',
            data: {
                action: 'getCities',
                countryID: countryID
            },
            success: function (data) {
                citiesArray = JSON.parse(data);
                citiesArray.forEach(function (city) {
                    let hotelDeclination = declination("отелей", "отель", "отеля", city['count_hotels']);
                    let div = document.createElement('div');
                    div.className = "formDirections__city oneCityRow";
                    $(div).attr('cty_id', city['id']);

                    div.innerHTML =
                        "<div class=\"formDirections__bottom-item\"><span class=\"formDirections__city\">" + city['name'] + "</span><span class=\"formDirections__count\">" + city['count_hotels'] + " " + hotelDeclination + "</span></div>";

                    //обработка клика на определенный город
                    $(div).click(function () {
                        orderData['resort'] = $(this).children('.formDirections__bottom-item').children('.formDirections__city').text();
                        orderData['resortID'] = $(this).attr('cty_id');
                        $('#htl_pull').html("");

                        //отправка запроса для построения списка отелей (по резорту)
                        $.ajax({
                            url: '/web/index.php?r=site/test',
                            type: 'post',
                            data: {
                                action: 'getHotels',
                                resortID: orderData['resortID']
                            },
                            success: function (response) {
                                let hotelArray = JSON.parse(response);

                                hotelArray.forEach(function (hotel) {
                                    let div2 = document.createElement('div');
                                    div2.className = "formDirections__bottom-item oneHotelRow";
                                    $(div2).attr('htl_id', hotel['id']);
                                    div2.innerHTML = "<span class='formDirections__city'><span class='formDirections__cut'>" + hotel['name'] + "</span></span><span class='formDirections__count'>"+city['name']+"</span>";

                                    //обработка клика по определенному отелю
                                    //скрываются определенные блоки информации
                                    $(div2).click(function () {
                                        orderData['hotel'] = $(this).children('.formDirections__city').children('.formDirections__cut').text();
                                        orderData['hotelID'] = $(this).attr('htl_id');
                                        $('#hotelStars').hide();
                                        $('#hotelStars2').hide();
                                        $('.bth__inp-country').hide();
                                        $('.bth__inp-city').text(orderData['hotel']);
                                        $('.formDirections').hide();
                                    });
                                    htl_pull.appendChild(div2);
                                });
                            }
                        });

                        $('.bth__inp-city').text(orderData['resort']).addClass('d-ib');
                        $('.js-act-city').removeClass('active');
                        $('.js-act-hotels').addClass('active').show();
                        $('.js-search-city').hide();
                        $('.js-search-hotels').show();

                    });
                    cty_pull.appendChild(div);
                });
            }
        });

        //показ выбранной страны
        $('.bth__img').attr("src", "/flags/" + imageName + ".jpg");
        $('.bth__inp-country').text(countryName).show();

        $('.bth__inp-city').removeClass('d-ib');
        $('.close_what').show();
        $('.js-act-city').show();

        $('.js-act-country').removeClass('active');
        $('.js-act-city').addClass('active');
        $('.js-search-country').hide();
        $('.js-search-city').show();
    });

    $('.countryName').click(function () {
        $('.otherWays').show();
        $('#hotelStars').show();
        $('#hotelStars2').show();
    });

    $('.close_what').click(function () {
        $('.bth__inp-city').html("&nbsp; Выбрать... ");
        $('.js-act-hotels').hide();
        $('.js-act-city').hide();
        $('.bth__inp-country').hide();
        $('.formDirections').hide();
        $('.js-search-hotels').hide();
        $('.js-search-city').hide();
        $('.js-search-country').show();

        if (!$('.bth__inp-city').hasClass('d-ib')) {
            $('.bth__inp-city').addClass('d-ib');
        }

        if ($('.bth__inp-flag').hasClass('d-ib')) {
            $('.bth__inp-flag').removeClass('d-ib');
        }

        orderData['hotel'] = orderData['resort'] = orderData['country'] = '';

        $('.otherWays').hide();
        $('#hotelStars').show();
        $('#hotelStars2').show();
    });

    //поиск страны
    $('#countrySearch').keyup(function () {
        let value = $(this).val();
        let reg;
        if (value !== '') {
            reg = RegExp("^" + value + "+");
        }
        else {
            reg = RegExp(".+");
        }

        $.each($('.js-show-flag'), function (index, element) {
            let name = $(element).children('.formDirections__city').text();
            if (name.search(reg) === -1) {
                $(element).hide();
            } else {
                $(element).show();
            }
        });

    });

    //поиск города
    $('#citySearch').keyup(function () {
        let value = $(this).val();
        let reg;
        if (value !== '') {
            reg = RegExp("^" + value + "+");
        } else {
            reg = RegExp(".+");
        }

        $.each($('.oneCityRow'), function (index, element) {
            let name = $(element).children('.formDirections__bottom-item').children('.formDirections__city').text();
            if (name.search(reg) === -1) {
                $(element).hide();
            } else {
                $(element).show();
            }
        });

    });

    //поиск отеля
    $('#hotelSearch').keyup(function () {
        let value = $(this).val();
        let reg;
        if (value !== '') {
            reg = RegExp("^" + value + "+");
        } else {
            reg = RegExp(".+");
        }

        $.each($('.oneHotelRow'), function (index, element) {
            let name = $(element).children('.formDirections__city').children('.formDirections__cut').text();
            if (name.search(reg) === -1) {
                $(element).hide();
            }
            else {
                $(element).show();
            }
        });

    });

    //отмена выбора отеля (выбор резорта)
    $('#cityTab').click(function () {
        $('.js-act-hotels').hide();
        orderData['hotel'] = '';
        $('#hotelStars').show();
        $('#hotelStars2').show();
        $('.bth__inp-city').text(orderData['resort']);

    });

    //отмена выбора отеля и резорта (выбор только страны)
    $('#countryTab').click(function () {
        $('.js-act-hotels').hide();
        $('.js-act-city').hide();
        orderData['hotel'] = orderData['resort'] = '';
        $('#hotelStars').show();
        $('#hotelStars2').show();
        $('.bth__inp-city').removeClass('d-ib');
        $('.bth__inp-country').show();
    });

    //показ окна с рейтингами
    $('#rateTab').click(function () {
        $(this).addClass('active');
        $('#commentTab').removeClass('active');
        $('.podtab_rate').show();
        $('.podtab_comment').hide();
    });

    //показ вкладки с отзывами
    $('#commentTab').click(function () {
        $(this).addClass('active');
        $('#rateTab').removeClass('active');
        $('.podtab_rate').hide();
        $('.podtab_comment').show();
    });

    //показ окна с рейтингами (корона)
    $('#rateHotelButton').click(function () {
        $('#rateTab').addClass('active');
        $('#commentTab').removeClass('active');
        $('.podtab_rate').show();
        $('.podtab_comment').hide();
    });

    //выбор конкретного чекбокса с рейтингом
    $('.rateCheckbox').click(function () {
        orderData['hotelRate'] = $.trim($(this).text());
        $('.formDirections').hide();
        updateRate();
    });

    $('#rate0').click(function () {
        orderData['hotelRate'] = '';
        $('.formDirections').hide();
        updateRate();
    });
    $('.reviewCheckbox').click(function () {
        orderData['hotelReviews'] = $.trim($(this).text());
        updateRate();
    });
    $('#rateReview0').click(function () {
        orderData['hotelReviews'] = '';
        updateRate();
    });
    $('#close').click(function () {
        $('.formDirections').hide();
        updateRate();
    });
    $('#closeRates').click(function () {
        $('.formDirections').hide();
        updateRate();
    });

    function updateRate() {
        let icon = "<img src=\"/i/tophotels/crown-green-20.png\" width=\"24\" height=\"18px\" class=\"mr5 va-top\">";
        let el = document.getElementById('last2years');
        let pseudoStyle = window.getComputedStyle(el, '::after');
        if (pseudoStyle['backgroundImage'] !== 'none' && orderData['hotelReviews'] !== '')
            orderData['hotelRate2years'] = 'за 2 года';
        else
            orderData['hotelRate2years'] = '';

        if (orderData['hotelRate'] === '' && orderData['hotelReviews'] === '')
            $('#rateHotelButton').html(icon + " рейтинг отеля");
        else
            $('#rateHotelButton').html(icon + " " + orderData['hotelRate'] + " " + orderData['hotelReviews'] + " " + orderData['hotelRate2years']);

    }

// клик финиша заполнения (3 шаг)
    $('#makeStepThree').click(function () {

        //сбор чекбоксов
        let otherDirections = document.getElementById('otherDirections');
        let otherDirectionsStyle = window.getComputedStyle(otherDirections, '::after');
        if (otherDirectionsStyle['backgroundImage'] !== 'none')
            orderData['otherDirections'] = 1;
        else
            orderData['otherDirections'] = 0;

        let is5star = document.getElementById('star5');
        let is5starStyle = window.getComputedStyle(is5star, '::after');
        if (is5starStyle['backgroundImage'] !== 'none')
            orderData['star5'] = 1;
        else
            orderData['star5'] = 0;

        let is4star = document.getElementById('star4');
        let is4starStyle = window.getComputedStyle(is4star, '::after');
        if (is4starStyle['backgroundImage'] !== 'none')
            orderData['star4'] = 1;
        else
            orderData['star4'] = 0;

        let is3star = document.getElementById('star3');
        let is3starStyle = window.getComputedStyle(is3star, '::after');
        if (is3starStyle['backgroundImage'] !== 'none')
            orderData['star3'] = 1;
        else
            orderData['star3'] = 0;

        let is2star = document.getElementById('star2');
        let is2starStyle = window.getComputedStyle(is2star, '::after');
        if (is2starStyle['backgroundImage'] !== 'none')
            orderData['star2'] = 1;
        else
            orderData['star2'] = 0;

        let line1 = document.getElementById('firstBeachLine');
        let line1Style = window.getComputedStyle(line1, '::after');
        if (line1Style['backgroundImage'] !== 'none')
            orderData['firstBeachLine'] = 1;
        else
            orderData['firstBeachLine'] = 0;

        let allinc = document.getElementById('allInclusive');
        let allincStyle = window.getComputedStyle(allinc, '::after');
        if (allincStyle['backgroundImage'] !== 'none')
            orderData['allInclusive'] = 1;
        else
            orderData['allInclusive'] = 0;

        let animation = document.getElementById('animation');
        let animationStyle = window.getComputedStyle(animation, '::after');
        if (animationStyle['backgroundImage'] !== 'none')
            orderData['animation'] = 1;
        else
            orderData['animation'] = 0;

        let party = document.getElementById('parties');
        let partyStyle = window.getComputedStyle(party, '::after');
        if (partyStyle['backgroundImage'] !== 'none')
            orderData['parties'] = 1;
        else
            orderData['parties'] = 0;

        let childPot = document.getElementById('childPot');
        let childPotStyle = window.getComputedStyle(childPot, '::after');
        if (childPotStyle['backgroundImage'] !== 'none')
            orderData['childPot'] = 1;
        else
            orderData['childPot'] = 0;

        let childFood = document.getElementById('childFood');
        let childFoodStyle = window.getComputedStyle(childFood, '::after');
        if (childFoodStyle['backgroundImage'] !== 'none')
            orderData['childFood'] = 1;
        else
            orderData['childFood'] = 0;

        let childTable = document.getElementById('childTable');
        let childTableStyle = window.getComputedStyle(childTable, '::after');
        if (childTableStyle['backgroundImage'] !== 'none')
            orderData['childTable'] = 1;
        else
            orderData['childTable'] = 0;

        let childAnimation = document.getElementById('childAnimation');
        let childAnimationStyle = window.getComputedStyle(childAnimation, '::after');
        if (childAnimationStyle['backgroundImage'] !== 'none')
            orderData['childAnimation'] = 1;
        else
            orderData['childAnimation'] = 0;

        orderData['addComment'] = $('#addComment').val();
        orderData['userCity'] = $('#userCity').val();
        orderData['userSubway'] = $('#userSubway').val();

        finalizeOrder()

        //переход на следующий шаг
        $('#step2Panel').slideUp(200);
        $('#formStep2Panel').slideDown(200);
    });

    //валидация поля email
    $('#Parammail').on('input', function () {
        if ($(this).val() === '' || !email_regex.test($(this).val())) {
            $(this).parent('.js-add-error').addClass('has-error');
        } else {
            $(this).parent('.js-add-error').removeClass('has-error');
        }
    })
    //валидация полей имя и телефон
    $('#Paramname, #Paramphone').on('input', function () {
        if ($(this).val() === '') {
            $(this).parent('.js-add-error').addClass('has-error');
        } else {
            $(this).parent('.js-add-error').removeClass('has-error');
        }
    });

    //отправка запроса на создание заявки
    //в ответ приходит ID созданной заявки
    function sendDataStage2() {
        $.ajax({
            url: '/web/index.php?r=site/test',
            type: 'post',
            data: {
                action: 'createId',
                name: orderData['name'],
                email: orderData['email'],
                phone: orderData['phone']
            },
            success: function (data) {
                orderData['orderId'] = data;

                //заявка дополняется информацией
                sendAdditionalData();
            },
            error: function () {
                alert('Произошла ошибка при создании заявки')
                window.location.reload();
            }
        });
    }

// обновление заявки в базе
    function sendAdditionalData() {
        let data = {
            startDate: orderData['startDate'],
            endDate: orderData['endDate'],
            counterDays: orderData['counterDays'],
            nightStart: orderData['fieldNightStart'],
            nightEnd: orderData['fieldNightEnd'],
            adults: orderData['adults'],
            children: orderData['children'],
            vacationType: orderData['vacationType'],
            currency: orderData['currency'],
            money: orderData['money'],
            name: orderData['name'],
            email: orderData['email'],
            phone: orderData['phone'],
            action: 'updateOrder',
            id: orderData['orderId']
        };

        $.ajax({
            url: '/web/index.php?r=site/test',
            type: 'post',
            async: false,
            data: data,
            success: function (response) {
                console.log(response);
            },
            error: function () {
                alert('Произошла ошибка при создании заявки')
                window.location.reload();
            }
        });
    }

    function finalizeOrder() {
        let data = {
            country: orderData['country'],
            countryID: orderData['countryID'],
            resort: orderData['resort'],
            resortID: orderData['resortID'],
            hotel: orderData['hotel'],
            hotelID: orderData['hotelID'],
            otherDirections: orderData['otherDirections'],
            firstBeachLine: orderData['firstBeachLine'],
            allInclusive: orderData['allInclusive'],
            animation: orderData['animation'],
            parties: orderData['parties'],
            childPot: orderData['childPot'],
            childFood: orderData['childFood'],
            childTable: orderData['childTable'],
            childAnimation: orderData['childAnimation'],
            addComment: orderData['addComment'],
            userCity: orderData['userCity'],
            userSubway: orderData['userSubway'],
            hotelRate: orderData['hotelRate'],
            hotelReviews: orderData['hotelReviews'],
            hotelRate2years: orderData['hotelRate2years'],
            star5: orderData['star5'],
            star4: orderData['star4'],
            star3: orderData['star3'],
            star2: orderData['star2'],
            action: 'finalizeOrder',
            id: orderData['orderId']
        };

        $.ajax({
            url: '/web/index.php?r=site/test',
            type: 'post',
            async: false,
            data: data,
            success: function (data) {
                console.log(data);
            }
        });
    }
});